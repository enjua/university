package University;

public enum Subjects{
	Informatics{
		@Override
		public String toString(){
			return "Информатика";
		}
	},
	Math{
		@Override
		public String toString(){
			return "Математика";
		}
	},

	RussianLanguage{
		@Override
		public String toString(){
			return "Русский язык";
		}
	},
	Physics{
		@Override
		public String toString(){
			return "Физика";
		}
	},
	History{
		@Override
		public String toString(){
			return "История";
		}
	},
	SocialStudies{
		@Override
		public String toString(){
			return "Обществознание";
		}
	},
	ICT{
		@Override
		public String toString(){
			return "Информационно-коммуникационные технологии";
		}
	}
}