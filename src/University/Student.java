package University;

import java.io.Serializable;
import java.util.Map;
import java.util.Objects;

public final class Student implements Serializable, Comparable<Student>{

	private static final long serialVersionUID = 2017051116L;


	private final String fullName;
	private final Map<Subjects,Integer> examResult;
	private final Integer privilegesCount;
	private Integer rating;

	private final boolean canEnterWithoutExam;

	public Student(String fullName, Map<Subjects,Integer> examResult, Integer privilegesCount, boolean canEnterWithoutExam){
		this.fullName = fullName;
		this.examResult = examResult;
		this.privilegesCount = privilegesCount;
		this.canEnterWithoutExam = canEnterWithoutExam;
	}

	public Integer getPrivilegesCount(){
		return privilegesCount;
	}

	public Integer getRating(){
		return rating;
	}

	public void setRating(Integer rating){
		this.rating = rating;
	}

	public String getFullName(){
		return fullName;
	}


	public Map<Subjects,Integer> getExamResult(){
		return examResult;
	}


	public boolean canEnterWithoutExam(){
		return canEnterWithoutExam;
	}

	public String getFullInfo(){
		StringBuilder builder = new StringBuilder();
		builder.append("Полное имя: ").append(this.fullName).append("\n");
		if(this.canEnterWithoutExam){
			builder.append("Поступил без вступительных испытаний (призер всероссийских оллимпиад)");
		} else{
			builder.append("Результаты экзаменов:\n");
			for(Map.Entry<Subjects,Integer> entry : this.examResult.entrySet()){
				builder.append("\t").append(entry.getKey()).append(" - ").append(entry.getValue()).append("\n");
			}
			builder.append("Количество льгот - ").append(this.getPrivilegesCount());
		}
		builder.append("\n");
		return builder.toString();
	}

	@Override
	public boolean equals(Object o){
		if(this == o)
			return true;
		if(!(o instanceof Student))
			return false;
		Student student = (Student) o;
		return canEnterWithoutExam == student.canEnterWithoutExam && Objects.equals(fullName, student.fullName) && Objects.equals(examResult, student.examResult) && Objects.equals(privilegesCount, student.privilegesCount) && Objects.equals(rating, student.rating);
	}

	@Override
	public int hashCode(){

		return Objects.hash(fullName, examResult, privilegesCount, rating, canEnterWithoutExam);
	}

	@Override
	public int compareTo(Student o){
		return o.getRating().compareTo(this.getRating());
	}
}
