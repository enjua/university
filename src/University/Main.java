package University;

import java.io.*;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

public class Main{

	private static final String BINARY_FILE_NAME = "rating.out";
	private static final Scanner scan = new Scanner(System.in);
	private static University university;
	private static Student student;
	private static Specialty specialty;
	private static Random random;

	public static void main(String[] args){
		System.out.println("Рейтинг абитуриентов\n");
		String temp = null;
		random = new Random();
		university = new University();
		loadFromBinaryFile();

		while(true){
			System.out.println("" + "Введите номер команды:\n" + "1 - добавить абитуриента\n" + "2 - рейтинг абитуриентов\n" + "3 - полная информация о абитуриенте\n" + "4 - закрыть программу");

			switch(Integer.parseInt(scan.nextLine())){
				case 1:{
					student = createStudent();
					specialty = getSpecialty();
					try{
						specialty.addStudent(student);
						System.out.println("Абитуриент " + student.getFullName() + " успешно добавлен.");
					} catch(IllegalArgumentException e){
						System.out.println("Не удалось добавить абитуриента.");
					}
					break;
				}
				case 2:{
					specialty = getSpecialty();
					specialty.printRating();
					break;
				}
				case 3:{
					System.out.println("Введите имя и фамилию абитуриента.");
					printInfoAboutStudent(scan.nextLine());
					break;
				}
				case 4:{
					saveToBinaryFile();
					return;
				}
				default:{
					continue;
				}
			}
		}
	}

	private static void generateTestData(){
		for(Specialty specialty : university){
			for(int i = 0; i < random.nextInt(2000); i++){
				try{
					specialty.addStudent(generateStudent());
				} catch(Exception e){
				}
			}
		}
	}


	//region create test data
	private static Student generateStudent(){
		return new Student(generateFullName(), generateRandomExamResult(), random.nextInt(3), generateRareEvent());
	}

	private static boolean generateRareEvent(){
		switch(random.nextInt(26)){
			case 1:{
			}
			case 2:{
			}
			case 3:{
			}
			case 4:{
			}
			case 5:{
			}
			case 6:{
			}
			case 7:{
			}
			case 8:{
			}
			case 9:{
			}
			case 10:{
			}
			case 11:{
			}
			case 12:{
			}
			case 13:{
			}
			case 14:{
			}
			case 15:{
			}
			case 16:{
			}
			case 17:{
			}
			case 18:{
			}
			case 19:{
			}
			case 20:{
			}
			case 21:{
			}
			case 22:{
			}
			case 23:{
			}
			case 24:{
				return false;
			}
			case 25:{
				return true;
			}
			default:{
				return false;
			}
		}
	}

	private static String generateFullName(){
		return generateFirstName() + " " + generateSecondName();
	}

	private static String generateSecondName(){
		switch(random.nextInt(6)){
			case 0:
				return "Иванов";
			case 1:
				return "Петров";
			case 2:
				return "Булгаков";
			case 3:
				return "Медведев";
			case 4:
				return "Федоров";
			case 5:
				return "Дуров";
			default:
				return "";
		}
	}

	private static String generateFirstName(){
		switch(random.nextInt(6)){
			case 0:
				return "Павел";
			case 1:
				return "Сергей";
			case 2:
				return "Егор";
			case 3:
				return "Дмитрий";
			case 4:
				return "Михаил";
			case 5:
				return "Андрей";
			default:
				return "";
		}
	}


	private static Map<Subjects,Integer> generateRandomExamResult(){
		int examResult = 0;
		Map<Subjects,Integer> result = new LinkedHashMap<>();
		for(Subjects subject : Subjects.values()){
			result.put(subject, random.nextInt(100));
		}
		return result;
	}

	//endregion

	private static void printInfoAboutStudent(String fullName){

		Specialty studentSpecialty = null;
		int count = 0;

		System.out.println();

		for(Specialty specialty : university){
			count = 0;
			for(Student student : specialty){
				count++;
				if(fullName.toLowerCase().equals(student.getFullName().toLowerCase())){
					System.out.println(specialty.getSpecialtyCode() + "\t" + specialty.getSpecialtyName());
					System.out.println("Место в рейтинге - " + count);
					System.out.println(student.getFullInfo());
					System.out.printf("");
				}
			}
		}
	}

	private static Student createStudent(){
		System.out.println("Введите имя и фамилию студента:");
		String name = scan.nextLine();
		System.out.println("Имеет ли абитуриент право на поступление без вступительных испытаний? (\"+\" если имеет)");
		boolean canEnterWithoutExam = scan.nextLine().contains("+");

		if(canEnterWithoutExam){
			return new Student(name, new LinkedHashMap<>(), 0, canEnterWithoutExam);
		}

		System.out.println("Введите результаты экзаменов абитуриента: \n(\"-\" если не сдавал)\n");
		Map<Subjects,Integer> examResult = getExamResult();
		System.out.println("Количество льгот, которые имеет абитуриент (кол-во пунктов в соответствии со списком льгот)");
		int privilegesCount = Integer.parseInt(scan.nextLine());
		return new Student(name, examResult, privilegesCount, canEnterWithoutExam);
	}

	private static Specialty getSpecialty(){
		System.out.println("Выберите номер специальности ");
		System.out.println("Номер\tКод\t\t\tНазвание");
		int count = 0;
		Specialty specialty = null;
		for(Map.Entry<String,Specialty> entry : university.getSpecialties().entrySet()){
			System.out.println(count++ + "\t\t" + entry.getKey() + "\t" + entry.getValue().getSpecialtyName());
		}
		System.out.println();
		int numberOfSpecialty = Integer.parseInt(scan.nextLine());
		for(Map.Entry<String,Specialty> entry : university.getSpecialties().entrySet()){
			if(numberOfSpecialty == 0){
				return entry.getValue();
			}
			numberOfSpecialty--;
		}
		return specialty;
	}

	private static Map<Subjects,Integer> getExamResult(){
		String examResult;
		Map<Subjects,Integer> result = new LinkedHashMap<>(6);
		System.out.println("Колличество баллов от 0 до 100 (или \"-\" чтобы пропустить)");
		for(Subjects subject : Subjects.values()){
			System.out.println(subject + ":");
			do{
				examResult = scan.nextLine();
				if(examResult.contains("-")){
					break;
				}
			} while(examResult.matches("[a-zA-Z]+"));
			if(examResult.contains("-")){
				continue;
			}
			result.put(subject, Integer.parseInt(examResult));
		}
		return result;
	}

	private static void saveToBinaryFile(){
		FileOutputStream fos = null;
		ObjectOutputStream oos = null;
		try{
			fos = new FileOutputStream(BINARY_FILE_NAME);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(university);
		} catch(IOException e){
			System.err.println("Error while store to binary file");
			e.printStackTrace();
		} finally{
			try{
				oos.flush();
				oos.close();
			} catch(IOException ex){
				System.err.println("Error while trying close IO");
				//    ex.printStackTrace();
			}
		}
	}


	private static void loadFromBinaryFile(){
		try{
			FileInputStream fis = new FileInputStream(BINARY_FILE_NAME);
			ObjectInputStream oin = new ObjectInputStream(fis);
			university = (University) oin.readObject();
		} catch(FileNotFoundException ex){
			//			System.out.println("Generate test");
			generateTestData();
		} catch(IOException e){
			e.printStackTrace();
			//			System.out.println("Generate test");
			generateTestData();
		} catch(ClassNotFoundException e){
			e.printStackTrace();
		}
	}

}