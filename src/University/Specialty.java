package University;

import java.io.Serializable;
import java.util.*;


public class Specialty implements Serializable, Iterable<Student>{

	private static final long serialVersionUID = 2017051118L;

	public static final int POINTS_FOR_PRIVILEGES = 20;

	private final String specialtyName;
	private final String specialtyCode;


	private final Integer budgetPlaces;
	private final Integer commercialPlaces;

	private ArrayList<Student> studentRating;
	private Map<Subjects,Integer> requiredExamResult;


	public Specialty(String specialtyName, String specialtyCode, Map<Subjects,Integer> requiredExamResult, int budgetPlaces, int commercialPlaces){
		this.specialtyName = specialtyName;
		this.specialtyCode = specialtyCode;


		this.studentRating = new ArrayList<Student>();
		this.requiredExamResult = requiredExamResult;

		this.budgetPlaces = budgetPlaces;
		this.commercialPlaces = commercialPlaces;
	}

	public String getSpecialtyName(){
		return specialtyName;
	}

	public String getSpecialtyCode(){
		return specialtyCode;
	}

	public void addStudent(Student student) throws IllegalArgumentException{
		student.setRating(calculateRating(student));
		studentRating.add(student);
	}

	private String getStudentInfo(Student student){
		StringBuilder builder = new StringBuilder();
		builder.append(student.getFullName());
		builder.append("\t\t");
		if(student.canEnterWithoutExam()){
			builder.append("Особое положение");
		} else{
			builder.append(student.getRating()).append("\t");
		}
		if(student.getPrivilegesCount() > 0){
			builder.append("\t").append("льгот - ").append(student.getPrivilegesCount());
		}
		return builder.toString();
	}


	public void printRating(){
		Collections.sort(this.studentRating);
		System.out.println("Рейтинг абитуриентов по специальности " + this.getSpecialtyName().toLowerCase());
		int count = 0;
		System.out.println("Бюджет:");
		for(Student student : studentRating){
			System.out.println(count++ + 1 + "\t" + getStudentInfo(student));
			if(count == this.budgetPlaces){
				System.out.println("--------------------------------------------");
				System.out.println("Контракт:");
			}
			if(count == this.budgetPlaces + this.commercialPlaces){
				System.out.println("--------------------------------------------");
			}
		}
		System.out.println();
	}

	private int calculateRating(Student student) throws IllegalArgumentException{
		if(student.canEnterWithoutExam()){
			return Integer.MAX_VALUE;
		}

		Map<Subjects,Integer> studentResult = student.getExamResult();
		Set<Subjects> requiredSubjects = requiredExamResult.keySet();
		int rating = 0;

		for(Subjects subject : requiredSubjects){

			if(!studentResult.containsKey(subject)){
				throw new IllegalArgumentException("Не сдан необходимый экзамен.");

			} else{

				if(studentResult.get(subject) < requiredExamResult.get(subject)){
					throw new IllegalArgumentException("По предмету " + subject + " не достигнут проходной балл.");
				} else{
					rating += studentResult.get(subject);
				}
			}
		}
		rating += student.getPrivilegesCount() * POINTS_FOR_PRIVILEGES;
		return rating;
	}

	@Override
	public Iterator<Student> iterator(){
		Collections.sort(studentRating);
		return this.studentRating.iterator();
	}
}
