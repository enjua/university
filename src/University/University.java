package University;

import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class University implements Serializable, Iterable<Specialty>{

	private static final long serialVersionUID = 2017051117L;
	private Map<String,Specialty> specialties;

	public University(){
		this.specialties = new LinkedHashMap(5);
		createSpecialty();
	}

	public Map<String,Specialty> getSpecialties(){
		return specialties;
	}

	private void createSpecialty(){
		Map<Subjects,Integer> exams = new LinkedHashMap<>(6);
		exams.put(Subjects.Math, 35);
		exams.put(Subjects.Physics, 35);
		exams.put(Subjects.RussianLanguage, 35);
		Specialty programIng = new Specialty("Программная инженерия", "09.03.04", exams, 20, 10);
		Specialty IST = new Specialty("Информационные системы и технологии", "09.03.02", exams, 40, 20);
		specialties.put(programIng.getSpecialtyCode(), programIng);
		specialties.put(IST.getSpecialtyCode(), IST);
	}


	@Override
	public Iterator<Specialty> iterator(){
		return specialties.values().iterator();
	}
}
